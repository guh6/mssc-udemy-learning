package learning.springframework.msscbreweryclient.web.client;

import learning.springframework.msscbreweryclient.web.model.BeerDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.net.URI;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class BreweryClientTest {

    @Autowired
    BreweryClient client;

    @Test
    void getBeerById() {
        BeerDto beerDto = client.getBeerById(UUID.randomUUID());

        assertNotNull(beerDto);
    }

    @Test
    void saveNewBeer() {
        BeerDto beerDto = BeerDto.builder().build();

        URI uri = client.saveNewBeer(beerDto);

        assertNotNull(uri);
    }

    @Test
    void updateBeer(){
        BeerDto beerDto = BeerDto.builder().build();
        UUID uuid = UUID.randomUUID();

        client.updateBeer(uuid, beerDto);
    }

    @Test
    void deleteBeer(){
        UUID uuid = UUID.randomUUID();
        client.deleteBeer(uuid);
    }
}