package learning.springframework.msscbrewery.services.v2;


import learning.springframework.msscbrewery.web.model.v2.BeerDtoV2;

import java.util.UUID;

/**
 * Created by Guoyang Huang on 2020-11-11
 */
public interface BeerServiceV2 {
    BeerDtoV2 getById(UUID beerId);

    BeerDtoV2 saveNewBeer(BeerDtoV2 beerDto);

    void updateBeer(UUID beerId, BeerDtoV2 beerDto);

    void deleteById(UUID beerId);
}
