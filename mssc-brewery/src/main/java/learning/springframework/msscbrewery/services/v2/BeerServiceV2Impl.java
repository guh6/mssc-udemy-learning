package learning.springframework.msscbrewery.services.v2;

import learning.springframework.msscbrewery.web.model.v2.BeerDtoV2;
import learning.springframework.msscbrewery.web.model.v2.BeerStyleEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class BeerServiceV2Impl implements BeerServiceV2 {

    @Override
    public BeerDtoV2 getById(UUID beerId) {
        return BeerDtoV2.builder()
                .id(beerId)
                .beerName("Galaxy Cat")
                .beerStyle(BeerStyleEnum.IPA)
                .build();
    }

    @Override
    public BeerDtoV2 saveNewBeer(BeerDtoV2 BeerDtoV2) {
        return BeerDtoV2.builder().
                id(UUID.randomUUID())
                .beerStyle(BeerDtoV2.getBeerStyle())
                .beerName(BeerDtoV2.getBeerName())
                .build();
    }

    @Override
    public void updateBeer(UUID beerId, BeerDtoV2 BeerDtoV2) {
        // todo impl
        log.debug("Updating Beer with Id={}", beerId);
    }

    @Override
    public void deleteById(UUID beerId) {
        // todo impl
        log.debug("Deleting Beer with Id={}", beerId);
    }
}
