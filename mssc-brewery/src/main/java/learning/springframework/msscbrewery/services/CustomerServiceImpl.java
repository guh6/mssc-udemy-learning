package learning.springframework.msscbrewery.services;

import learning.springframework.msscbrewery.web.model.CustomerDto;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class CustomerServiceImpl implements CustomerService{
    @Override
    public CustomerDto getById(UUID uuid){
        return CustomerDto.builder()
                .id(uuid)
                .name("John George")
                .build();
    }

    @Override
    public CustomerDto saveNewCustomer(CustomerDto customerDto) {
        return CustomerDto.builder()
                .id(UUID.randomUUID())
                .name(customerDto.getName())
                .build();
    }

    @Override
    public void updateCustomer(UUID customerId, CustomerDto customerDto) {
        // todo;
    }

    @Override
    public void deleteById(UUID customerId) {
        // todo;
    }
}
