package learning.springframework.msscbrewery.web.controller;

import learning.springframework.msscbrewery.services.CustomerService;
import learning.springframework.msscbrewery.web.model.CustomerDto;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Guoyang Huang on 2020-11-10
 */
@RequestMapping("/api/v1/customer")
@RestController
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping({"/{customerUuid}"})
    public ResponseEntity<CustomerDto> getById(@PathVariable(value = "customerUuid") final UUID customerUuid) {
        return ResponseEntity.ok(customerService.getById(customerUuid));
    }

    @PostMapping
    public ResponseEntity<Void> createCustomer(@Valid  @RequestBody CustomerDto customerDto) {
        CustomerDto savedDto = customerService.saveNewCustomer(customerDto);

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.LOCATION, "/api/v1/customer/" + customerDto.getId());

        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @PutMapping("/{customerId}")
    public ResponseEntity<Void> updateCustomer(@PathVariable(value = "customerId") UUID customerId,
                                               @Valid @RequestBody CustomerDto customerDto) {
        customerService.updateCustomer(customerId, customerDto);

        return ResponseEntity.noContent().build();
    }

// SEE MvcExceptionHandler - the global controller advice
//    @DeleteMapping("/{customerId}")
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    public void deleteCustomer(@PathVariable("customerId") UUID customerId) {
//        customerService.deleteById(customerId);
//    }
//
//    @ExceptionHandler(ConstraintViolationException.class)
//    public ResponseEntity<List<String>> handleConstraintViolations(ConstraintViolationException e){
//        List<String> errors = new ArrayList<>(e.getConstraintViolations().size());
//
//        e.getConstraintViolations().forEach(v -> errors.add(v.getPropertyPath() + " : " + v.getMessage()));
//
//        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
//
//    }
}
