package learning.springframework.msscbrewery.web.controller.v2;

import learning.springframework.msscbrewery.services.v2.BeerServiceV2;
import learning.springframework.msscbrewery.web.model.v2.BeerDtoV2;
import lombok.val;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Guoyang Huang on 2020-11-11
 */
@RequestMapping("/api/v2/beer")
@RestController
public class BeerControllerV2 {
    private final BeerServiceV2 beerService;

    public BeerControllerV2(BeerServiceV2 beerService) {
        this.beerService = beerService;
    }

    @GetMapping({"/{beerId}"})
    public ResponseEntity<BeerDtoV2> getBeer(@PathVariable("beerId") UUID beerId){
        return ResponseEntity.ok(beerService.getById(beerId));
    }

    @PostMapping
    public ResponseEntity<Void> createBeer(@Valid @RequestBody BeerDtoV2 BeerDtoV2) {
//        BeerDtoV2 savedDto = beerService.saveNewBeer(BeerDtoV2);
        val savedDto = beerService.saveNewBeer(BeerDtoV2);

//        HttpHeaders headers = new HttpHeaders();
        val headers = new HttpHeaders();

        // todo add hostname to url so we can use ResponseEntity.created(URI)
        headers.add(HttpHeaders.LOCATION, "/api/v1/beer/" + savedDto.getId().toString());

        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @PutMapping("/{beerId}")
    public ResponseEntity<Void> updateBeer(@PathVariable("beerId") UUID beerId,
                                           @Valid @RequestBody BeerDtoV2 BeerDtoV2) {
        beerService.updateBeer(beerId, BeerDtoV2);

        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{beerId}")
    @ResponseStatus(HttpStatus.NO_CONTENT) // Another way to do ResponseEntity<Void>
    public void deleteBeer(@PathVariable("beerId") UUID beerId) {
        beerService.deleteById(beerId);
    }

// SEE MvcExceptionHandler - the global controller advice
//    /**
//     * Custom Validation exception handler
//     * @return validation errors
//     */
//    @ExceptionHandler(ConstraintViolationException.class)
//    public ResponseEntity<List<String>> validationErrorHandler(ConstraintViolationException e){
//        List<String> errors = new ArrayList<>(e.getConstraintViolations().size());
//        e.getConstraintViolations().forEach(v -> {
//            errors.add(v.getPropertyPath() + " : " + v.getMessage());
//        });
//
//        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
//    }
}
