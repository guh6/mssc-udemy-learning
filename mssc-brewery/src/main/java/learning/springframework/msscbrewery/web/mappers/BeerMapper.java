package learning.springframework.msscbrewery.web.mappers;

import learning.springframework.msscbrewery.domain.Beer;
import learning.springframework.msscbrewery.web.model.BeerDto;
import org.mapstruct.Mapper;

/**
 * Created by GUoyang Huang on 2020-11-13
 */
@Mapper(uses = {DateMapper.class})
public interface BeerMapper {
    BeerDto beertoBeerDto(Beer beer);

    // This is the inverse
    Beer beerDtoToBeer(BeerDto dto);
}
