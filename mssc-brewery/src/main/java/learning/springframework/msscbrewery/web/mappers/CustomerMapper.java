package learning.springframework.msscbrewery.web.mappers;

import learning.springframework.msscbrewery.domain.Customer;
import learning.springframework.msscbrewery.web.model.CustomerDto;
import org.mapstruct.Mapper;

@Mapper
public interface CustomerMapper {
    CustomerDto customerToCustomerDto(Customer customer);

    Customer customerDtoToCustomer(CustomerDto customerDto);
}
