package learning.springframework.msscbrewery.web.mappers;

import lombok.val;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

/**
 * Created by Guoyang Huang on 2020-11-13
 *
 * This is to demonstrate the conversion of
 *  Timestamp(Entity, DB limitation) -> OffsetDateTime(DTO, client facing)
 */
@Component
public class DateMapper {
    public OffsetDateTime asOffsetDateTime(Timestamp ts) {
        if(ts != null) {
            val localDateTime = ts.toLocalDateTime();
            return OffsetDateTime.of(localDateTime.getYear(), localDateTime.getMonthValue(), localDateTime.getDayOfMonth(),
                    localDateTime.getHour(), localDateTime.getMinute(), localDateTime.getSecond(), localDateTime.getNano(),
                    ZoneOffset.UTC);
        } else {
            return null;
        }
    }

    public Timestamp asTimestamp(OffsetDateTime offsetDateTime) {
        if(offsetDateTime != null) {
            return Timestamp.valueOf(offsetDateTime.atZoneSameInstant((ZoneOffset.UTC)).toLocalDateTime());
        } else {
            return null;
        }
    }
}
