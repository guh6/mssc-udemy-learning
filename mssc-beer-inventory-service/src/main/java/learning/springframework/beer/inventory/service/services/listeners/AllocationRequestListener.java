package learning.springframework.beer.inventory.service.services.listeners;

import learning.springframework.beer.inventory.service.config.JmsConfig;
import learning.springframework.beer.inventory.service.services.AllocationService;
import learning.springframework.common.model.AllocateOrderRequest;
import learning.springframework.common.model.AllocateOrderResult;
import learning.springframework.common.model.BeerOrderDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class AllocationRequestListener {
    private final AllocationService allocationService;
    private final JmsTemplate jmsTemplate;

    @JmsListener(destination = JmsConfig.QUEUE_ALLOCATE_ORDER)
    public void listen(AllocateOrderRequest allocateOrderRequest){
        BeerOrderDto beerOrderDto = allocateOrderRequest.getBeerOrderDto();

        AllocateOrderResult.AllocateOrderResultBuilder allocateOrderResultBuilder = AllocateOrderResult.builder();
        try {
            final boolean result = allocationService.allocateOrder(beerOrderDto);
            allocateOrderResultBuilder.pendingInventory(result);
        } catch (Exception e) {
            log.error("Allocation failed for order id={}", allocateOrderRequest.getBeerOrderDto().getId(), e);
            allocateOrderResultBuilder.allocationError(true);
        }

        jmsTemplate.convertAndSend(JmsConfig.QUEUE_ALLOCATE_ORDER_RESULT,
                allocateOrderResultBuilder.build());

    }
}
