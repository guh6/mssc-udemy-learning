package learning.springframework.beer.inventory.service.services;

import learning.springframework.beer.inventory.service.config.JmsConfig;
import learning.springframework.beer.inventory.service.domain.BeerInventory;
import learning.springframework.beer.inventory.service.repositories.BeerInventoryRepository;
import learning.springframework.common.events.NewInventoryEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class NewInventoryListener {
    private final BeerInventoryRepository beerInventoryRepository;

    @JmsListener(destination = JmsConfig.QUEUE_NEW_INVENTORY)
    public void listen(NewInventoryEvent event) {
        log.debug("Received new inventory event={}. Saving to Db.", event);
        beerInventoryRepository.save(BeerInventory.builder()
                .beerId(event.getBeerDto().getId())
                .upc(event.getBeerDto().getUpc())
                .quantityOnHand(event.getBeerDto().getQuantityOnHand())
                .build());
    }
}
