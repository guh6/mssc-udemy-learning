package learning.springframework.beer.inventory.service.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;

@Configuration
public class TaskConfig {
    @Bean
    public TaskExecutor executor(){
        return new SimpleAsyncTaskExecutor();
    }
}
