package learning.springframework.beer.inventory.service.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

@Configuration
public class JmsConfig {

    public static final String QUEUE_NEW_INVENTORY="new-inventory";
    public static final String QUEUE_ALLOCATE_ORDER = "allocate-order";
    public static final String QUEUE_ALLOCATE_ORDER_RESULT = "allocate-order-result";

    @Bean
    public MessageConverter messageConverter(ObjectMapper objectMapper){
        // This will be used for serialization and deserialization
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        converter.setObjectMapper(objectMapper);
        return converter;
    }
}
