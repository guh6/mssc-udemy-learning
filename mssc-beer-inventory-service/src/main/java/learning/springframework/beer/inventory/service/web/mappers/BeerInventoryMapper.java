package learning.springframework.beer.inventory.service.web.mappers;

import learning.springframework.beer.inventory.service.domain.BeerInventory;
import learning.springframework.common.model.BeerInventoryDto;
import org.mapstruct.Mapper;

/**
 * Created by jt on 2019-05-31.
 */
@Mapper(uses = {learning.springframework.beer.inventory.service.web.mappers.DateMapper.class})
public interface BeerInventoryMapper {

    BeerInventory beerInventoryDtoToBeerInventory(BeerInventoryDto beerInventoryDTO);

    BeerInventoryDto beerInventoryToBeerInventoryDto(BeerInventory beerInventory);
}
