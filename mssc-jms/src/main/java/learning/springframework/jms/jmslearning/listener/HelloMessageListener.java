package learning.springframework.jms.jmslearning.listener;

import learning.springframework.jms.jmslearning.config.JmsConfig;
import learning.springframework.jms.jmslearning.model.HelloWorldMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import java.util.UUID;

@RequiredArgsConstructor
@Component // Comment out the Component if you want to see the message in Artemis Dashboard
@Slf4j
public class HelloMessageListener {

    /**
     * This is to demonstrate receiving a message without replying!
     * @param helloWorldMessage @Payload tells it to deserialize
     * @param headers           @Headers tells it to get the message header
     * @param message
     */
    @JmsListener(destination = JmsConfig.MY_QUEUE)
    public void listen(@Payload HelloWorldMessage helloWorldMessage,
                       @Headers MessageHeaders headers, Message message) {
        log.info("listen message: {}", helloWorldMessage);
        // If there is an exception here then the message will get re-queued and
        // and re-delivered because the process is transactional
    }

///////////////////////////////////////////////////////////////////////////////

    private final JmsTemplate jmsTemplate;

    /**
     * This is to demonstrate replying to a received message!
     * @param helloWorldMessage
     * @param headers
     * @param message
     * @throws JMSException
     */
    @JmsListener(destination = JmsConfig.MY_SEND_AND_RECEIVE_QUEUE)
    public void listenForHello(@Payload HelloWorldMessage helloWorldMessage,
                               @Headers MessageHeaders headers, Message message) throws JMSException {
        HelloWorldMessage payloadMsg = HelloWorldMessage.builder()
                .id(UUID.randomUUID())
                .message("World!")
                .build();
        log.info("listenForHello message: {}", helloWorldMessage);
        jmsTemplate.convertAndSend(message.getJMSReplyTo(), payloadMsg);
    }

}
