package learning.springframework.jms.jmslearning.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
// Serializable is mandatory if messages is sent between Java processes
public class HelloWorldMessage implements Serializable {
    // If sent as TextMessage via JSON we do not need this, otherwise if
    // we are sending Java Objects the nwe will need Serializable + serialVersionUID.
    static final long serialVersionUID = -12902834902834L;

    private UUID id;
    private String message;
}
