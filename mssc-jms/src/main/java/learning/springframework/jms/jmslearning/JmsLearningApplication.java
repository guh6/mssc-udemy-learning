package learning.springframework.jms.jmslearning;

//import org.apache.activemq.artemis.core.config.impl.ConfigurationImpl;
//import org.apache.activemq.artemis.core.server.ActiveMQServer;
//import org.apache.activemq.artemis.core.server.ActiveMQServers;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JmsLearningApplication {

	public static void main(String[] args) throws Exception {

// (2/2)
//		// Let's start our server with this code below
//		// This sets up a minimal ActiveMQ server - ideally this will be a
//		// separate instance outside of Spring boot
//		ActiveMQServer server = ActiveMQServers.newActiveMQServer(new ConfigurationImpl()
//			.setPersistenceEnabled(false)
//			.setJournalDirectory("build/data/journal")
//			.setSecurityEnabled(false)
//			.addAcceptorConfiguration("invm", "vm://1"));
//		server.start();

		SpringApplication.run(JmsLearningApplication.class, args);
	}

}
