package learning.springframework.msscbeerservice.msscbeerservice.mappers;

import learning.springframework.msscbeerservice.msscbeerservice.domain.Beer;
import learning.springframework.common.model.BeerDto;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;

@Mapper(uses = {DateMapper.class})
@DecoratedWith(BeerMapperDecorator.class)
public interface BeerMapper {
    BeerDto beerToBeerDto(Beer beer);

    BeerDto beerToBeerDtoWithOnHandInventory(Beer beer);

    Beer beerDtoToBeer(BeerDto beerDto);

    Beer beerDtoToBeerWithOnHandInventory(BeerDto beerDto);

}
