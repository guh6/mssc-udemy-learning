package learning.springframework.msscbeerservice.msscbeerservice.services.order;

import learning.springframework.common.model.BeerOrderDto;
import learning.springframework.common.model.ValidateBeerOrderRequest;
import learning.springframework.common.model.ValidateOrderResult;
import learning.springframework.msscbeerservice.msscbeerservice.config.JmsConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import static learning.springframework.msscbeerservice.msscbeerservice.config.JmsConfig.QUEUE_NEW_INVENTORY;

@Slf4j
@Service
@RequiredArgsConstructor
public class ValidateOrderRequestListener {
    private final OrderRequestValidator orderRequestValidator;
    private final JmsTemplate jmsTemplate;

    @Transactional
    @JmsListener(destination = JmsConfig.QUEUE_VALIDATE_ORDER)
    public void listen(ValidateBeerOrderRequest validateBeerOrderRequest) {
        BeerOrderDto beerOrderDto = validateBeerOrderRequest.getBeerOrderDto();
        log.debug("Received {}. to validate order request for {}", JmsConfig.QUEUE_VALIDATE_ORDER, beerOrderDto);

        ValidateOrderResult validateOrderResult = ValidateOrderResult.builder()
                .orderId(beerOrderDto.getId())
                .isValid(orderRequestValidator.validateOrder(beerOrderDto))
                .build();

        jmsTemplate.convertAndSend(QUEUE_NEW_INVENTORY, JmsConfig.QUEUE_VALIDATE_ORDER_RESULT);
    }
}

