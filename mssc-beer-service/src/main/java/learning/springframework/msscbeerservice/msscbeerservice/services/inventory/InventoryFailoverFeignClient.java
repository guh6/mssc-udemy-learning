//package learning.springframework.msscbeerservice.msscbeerservice.services.inventory;
//
//import learning.springframework.msscbeerservice.msscbeerservice.services.inventory.model.BeerInventoryDto;
//import org.springframework.cloud.netflix.feign.FeignClient;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//
//import java.util.UUID;
//
///**
// * This is to demo the inventory-failover service for circuit breaker
// */
//@FeignClient(name = "inventory-failover")
//public interface InventoryFailoverFeignClient {
//
//    @RequestMapping(method = RequestMethod.GET, value = "/inventory-failover")
//    ResponseEntity<BeerInventoryDto> getOnhandInventory(UUID beerId);
//
//}
