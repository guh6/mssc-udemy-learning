package learning.springframework.msscbeerservice.msscbeerservice.services.brewing;

import learning.springframework.msscbeerservice.msscbeerservice.domain.Beer;
import learning.springframework.common.events.BrewBeerEvent;
import learning.springframework.msscbeerservice.msscbeerservice.mappers.BeerMapper;
import learning.springframework.msscbeerservice.msscbeerservice.repository.BeerRepository;
import learning.springframework.msscbeerservice.msscbeerservice.services.inventory.BeerInventoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

import static learning.springframework.msscbeerservice.msscbeerservice.config.JmsConfig.QUEUE_BREWING_REQUEST;

/**
 * This Service checks for inventory vs beer's minOnHand and creates a message
 * to brew new beers if the inventory on hand is running low.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class BrewingService {
    private final BeerRepository beerRepository;
    private final BeerInventoryService beerInventoryService;
    private final JmsTemplate jmsTemplate;
    private final BeerMapper beerMapper;

    @Scheduled(fixedRate = 5000)
    public void checkForLowInventory(){
        List<Beer> beers = beerRepository.findAll();
        beers.forEach(beer -> {
            Integer inventoryQuantityOnHand = beerInventoryService.getOnhandInventory(beer.getId());
            log.debug("Beer={} inventoryQuantityOnHand={} vs minOnHand={}", beer.getId(),
                    inventoryQuantityOnHand, beer.getMinOnHand());

            if(beer.getMinOnHand() >= inventoryQuantityOnHand) {
                jmsTemplate.convertAndSend(QUEUE_BREWING_REQUEST, new BrewBeerEvent(beerMapper.beerToBeerDto(beer)));
                log.debug("Message sent to {}", QUEUE_BREWING_REQUEST);
            }
        });
    }
}
