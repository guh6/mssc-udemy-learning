package learning.springframework.msscbeerservice.msscbeerservice.bootstrap;

import learning.springframework.msscbeerservice.msscbeerservice.domain.Beer;
import learning.springframework.msscbeerservice.msscbeerservice.repository.BeerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * Created by Guoyang Huang on 2020-11-13
 *
 * This will run whenever Spring Context is loaded
 */
@Component
@Slf4j
public class BeerLoader implements CommandLineRunner {
    public static final String BEER_1_UPC = "0631234200036";
    public static final String BEER_2_UPC = "0631234300019";
    public static final String BEER_3_UPC = "0083783375213";

    private final BeerRepository beerRepository;

    public BeerLoader(BeerRepository beerRepository) {
        this.beerRepository = beerRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        if(beerRepository.count() == 0) {
            loadBeerObjects();
        }
    }

    private void loadBeerObjects() {
        beerRepository.save(Beer.builder()
                .beerName("Mango Bobs")
                .beerStyle("IPA")
                .minOnHand(12)
                .quantityToBrew(200)
                .upc(BEER_1_UPC)
                .price(new BigDecimal("12.95"))
                .build());
        log.debug("Created Mango Bobs Traditional Lager beer.");


        beerRepository.save(Beer.builder()
                .beerName("Yuengling Traditional Lager")
                .beerStyle("LAGER")
                .minOnHand(12)
                .quantityToBrew(200)
                .upc(BEER_2_UPC)
                .price(new BigDecimal("20.99"))
                .build());
        log.debug("Created Yuengling Traditional Lager beer.");

        beerRepository.save(Beer.builder()
                .beerName("No Hammers On The Bar")
                .beerStyle("PALE_ALE")
                .minOnHand(12)
                .quantityToBrew(200)
                .upc(BEER_3_UPC)
                .price(new BigDecimal("20.99"))
                .build());
        log.debug("Created Yuengling Traditional Lager beer.");
    }
}
