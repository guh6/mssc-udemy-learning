//package learning.springframework.msscbeerservice.msscbeerservice.services.inventory;
//
//import learning.springframework.msscbeerservice.msscbeerservice.services.inventory.model.BeerInventoryDto;
//import org.springframework.cloud.netflix.feign.FeignClient;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//
//import java.util.UUID;
//
//@FeignClient(name = "inventory-service", fallback = InventoryFailoverFeignClientImpl.class) // Eureka app name
//public interface InventoryServiceFeignClient {
//    @RequestMapping(method = RequestMethod.GET, value = BeerInventoryServiceRestTemplateImpl.INVENTORY_PATH)
//    ResponseEntity<BeerInventoryDto> getOnhandInventory(UUID beerId);
//}
