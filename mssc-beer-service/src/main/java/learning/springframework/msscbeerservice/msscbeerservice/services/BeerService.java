package learning.springframework.msscbeerservice.msscbeerservice.services;

import learning.springframework.common.model.BeerDto;
import learning.springframework.common.model.BeerPagedList;
import learning.springframework.common.model.BeerStyleEnum;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.UUID;

public interface BeerService {
    List<BeerDto> getAll();

    BeerDto getById(UUID beerId, boolean showInventoryOnHand);

    BeerDto updateBeer(UUID beerId, BeerDto beerDto);
    BeerDto saveNewBeer(BeerDto beerDto);

    BeerPagedList listBeers(String beerName, BeerStyleEnum beerStyle, PageRequest of, boolean showInventoryOnHand);

    BeerDto getByUpc(String upc, boolean showInventoryOnHand);
}
