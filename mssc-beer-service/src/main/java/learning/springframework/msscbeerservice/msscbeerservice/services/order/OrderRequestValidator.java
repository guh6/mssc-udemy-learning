package learning.springframework.msscbeerservice.msscbeerservice.services.order;

import learning.springframework.common.model.BeerOrderDto;
import learning.springframework.msscbeerservice.msscbeerservice.repository.BeerRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@RequiredArgsConstructor
@Component
public class OrderRequestValidator {
    private final BeerRepository beerRepository;

    /**
     * Look through the line order to see if the beer exists
     * @param beerOrderDto
     * @return
     */
    public Boolean validateOrder(BeerOrderDto beerOrderDto) {
        AtomicInteger beersNotFound = new AtomicInteger();
        beerOrderDto.getBeerOrderLines().forEach(o -> {
            if (beerRepository.findByUpc(o.getUpc()) == null) {
                beersNotFound.incrementAndGet();
            }
        });

        return beersNotFound.get() == 0;
    }
}
