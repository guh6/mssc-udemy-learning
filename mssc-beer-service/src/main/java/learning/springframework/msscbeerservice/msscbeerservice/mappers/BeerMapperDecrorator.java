package learning.springframework.msscbeerservice.msscbeerservice.mappers;

import learning.springframework.msscbeerservice.msscbeerservice.domain.Beer;
import learning.springframework.msscbeerservice.msscbeerservice.services.inventory.BeerInventoryService;
import learning.springframework.common.model.BeerDto;
import org.springframework.beans.factory.annotation.Autowired;

abstract class BeerMapperDecorator implements BeerMapper {
    private BeerInventoryService beerInventoryService;
    private BeerMapper mapper;

    @Autowired
    public void setBeerInventoryService(BeerInventoryService beerInventoryService) {
        this.beerInventoryService = beerInventoryService;
    }

    @Autowired
    public void setMapper(BeerMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public BeerDto beerToBeerDto(Beer beer) {
        BeerDto dto = mapper.beerToBeerDto(beer);
        return dto;
    }

    @Override
    public BeerDto beerToBeerDtoWithOnHandInventory(Beer beer) {
        BeerDto dto = mapper.beerToBeerDto(beer);
        // This does additional conversion
        dto.setQuantityOnHand(beerInventoryService.getOnhandInventory(beer.getId()));
        return dto;
    }

    @Override
    public Beer beerDtoToBeer(BeerDto beerDto) {
        return mapper.beerDtoToBeer(beerDto);
    }
}