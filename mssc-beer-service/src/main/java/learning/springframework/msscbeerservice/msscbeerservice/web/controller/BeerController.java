package learning.springframework.msscbeerservice.msscbeerservice.web.controller;

import learning.springframework.msscbeerservice.msscbeerservice.services.BeerService;
import learning.springframework.common.model.BeerDto;
import learning.springframework.common.model.BeerPagedList;
import learning.springframework.common.model.BeerStyleEnum;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping("/api/v1/beer")
@RestController
@RequiredArgsConstructor // Since Spring 4.2, we no longer need @Autowired
public class BeerController {
    private static final Integer DEFAULT_PAGE_NUMBER = 0;
    private static final Integer DEFAULT_PAGE_SIZE = 25;
    private final BeerService beerService;

//    @GetMapping(produces = "application/json")
//    public ResponseEntity<List<BeerDto>> getAll() {
//        return new ResponseEntity<>(beerService.getAll(), HttpStatus.OK);
//    }

    @GetMapping(produces = "application/json")
    public ResponseEntity<BeerPagedList> getAll(
            @RequestParam(value = "pageNumber", required = false) Integer pageNumber,
            @RequestParam(value = "pageSize", required = false) Integer pageSize,
            @RequestParam(value = "beerName", required = false) String beerName,
            @RequestParam(value = "beerStyle", required = false) BeerStyleEnum beerStyle,
            @RequestParam(value = "showInventoryOnHand", required = false) boolean showInventoryOnHand){

        if(pageNumber == null || pageNumber < 0) {
            pageNumber = DEFAULT_PAGE_NUMBER;
        }

        if(pageSize == null || pageSize < 1) {
            pageSize = DEFAULT_PAGE_SIZE;
        }

        BeerPagedList beerPagedList = beerService.listBeers(beerName, beerStyle, PageRequest.of(pageNumber, pageSize), showInventoryOnHand);

        return new ResponseEntity<>(beerPagedList, HttpStatus.OK);

    }

    @GetMapping("/{beerId}")
    public ResponseEntity<BeerDto> getById(@PathVariable("beerId")UUID beerId,
                                           @RequestParam(value = "showInventoryOnHand", required = false) boolean showInventoryOnHand) {
        BeerDto beerDto = beerService.getById(beerId, showInventoryOnHand);
        if(beerDto == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(beerDto);
    }

    @GetMapping("/beerUpc/{upc}")
    public ResponseEntity<BeerDto> getByUpc(@PathVariable("upc") final String upc,
                                            @RequestParam(value = "showInventoryOnHand", required = false) boolean showInventoryOnHand) {
        BeerDto beerDto = beerService.getByUpc(upc, showInventoryOnHand);
        if(beerDto == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(beerDto);
    }

    @PostMapping
    public ResponseEntity<BeerDto> saveNewBeer(@Valid @RequestBody BeerDto beerDto) {
        BeerDto savedBeerDto = beerService.saveNewBeer(beerDto);
        return new ResponseEntity<>(savedBeerDto, HttpStatus.CREATED);
    }

    @PutMapping("/{beerId}")
    public ResponseEntity<BeerDto> updateById(@PathVariable("beerId") UUID beerId, @Valid @RequestBody BeerDto beerDto) {
       BeerDto updatedBeerDto = beerService.updateBeer(beerId, beerDto);
       // TODO - Not sure if 204 is acceptable here. Maybe 200 if we are returning a body?
       return new ResponseEntity<>(updatedBeerDto, HttpStatus.NO_CONTENT);
    }

}
