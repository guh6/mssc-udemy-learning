package learning.springframework.msscbeerservice.msscbeerservice.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

@Configuration
public class JmsConfig {
    public static final String QUEUE_BREWING_REQUEST = "brewing-request";
    public static final String QUEUE_NEW_INVENTORY = "new-inventory";
    public static final String QUEUE_VALIDATE_ORDER_RESULT = "validate-order-result";
    public static final String QUEUE_VALIDATE_ORDER = "validate-order";

    @Bean
    public MessageConverter messageConverter(ObjectMapper objectMapper){
        // This will be used for serialization and deserialization
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        converter.setObjectMapper(objectMapper); // Uses Spring Boot's managed object mapper for converting OffsetDateTime
        return converter;
    }
}
