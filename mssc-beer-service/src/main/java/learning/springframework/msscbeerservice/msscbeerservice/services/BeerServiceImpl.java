package learning.springframework.msscbeerservice.msscbeerservice.services;

import learning.springframework.msscbeerservice.msscbeerservice.domain.Beer;
import learning.springframework.msscbeerservice.msscbeerservice.mappers.BeerMapper;
import learning.springframework.msscbeerservice.msscbeerservice.repository.BeerRepository;
import learning.springframework.common.model.BeerDto;
import learning.springframework.common.model.BeerPagedList;
import learning.springframework.common.model.BeerStyleEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Slf4j
@Service
public class BeerServiceImpl implements BeerService {

    private final BeerRepository beerRepository;
    private final BeerMapper beerMapper;

    @Override
    public List<BeerDto> getAll(){
        return beerRepository.findAll().stream()
                .map(beerMapper::beerToBeerDto).collect(Collectors.toList());
    }

    @Override
    // The cache key is explicitly created from the key attribute value
    @Cacheable(cacheNames = "beerCache", key = "#beerId", condition = "#showInventoryOnHand == false")
    public BeerDto getById(UUID beerId, boolean showInventoryOnHand) {
        log.debug("I was called");
        Optional<Beer> beer = beerRepository.findById(beerId);
        if(showInventoryOnHand) {
            return beer.map(beerMapper::beerToBeerDtoWithOnHandInventory).orElse(null);
        } else {
            return beer.map(beerMapper::beerToBeerDto).orElse(null);
        }
    }

    @Override
    @Cacheable(cacheNames = "beerUpcCache", key = "#upc", condition = "#showInventoryOnHand == false")
    public BeerDto getByUpc(String upc, boolean showInventoryOnHand) {
        log.debug("I was called");
        Optional<Beer> beer = beerRepository.findByUpc(upc);
        if(showInventoryOnHand) {
            return beer.map(beerMapper::beerToBeerDtoWithOnHandInventory).orElse(null);
        } else {
            return beer.map(beerMapper::beerToBeerDto).orElse(null);
        }
    }

    @Override
    public BeerDto saveNewBeer(BeerDto beerDto) {
        Beer savedBeer = beerRepository.save(beerMapper.beerDtoToBeer(beerDto));
        return beerMapper.beerToBeerDto(savedBeer);
    }

    @Override
    // The cache key is created by the method parameters
    @Cacheable(cacheNames = "beerListCache", condition = "#showInventoryOnHand == false")
    public BeerPagedList listBeers(String beerName, BeerStyleEnum beerStyle, PageRequest pageRequest, boolean showInventoryOnHand) {
        log.debug("I was called");

        BeerPagedList beerPagedList;
        Page<Beer> beerPage;

        if(StringUtils.isNotBlank(beerName) && beerStyle != null) {
            // search both
            beerPage = beerRepository.findAllByBeerNameAndBeerStyle(beerName, beerStyle, pageRequest);
        } else if (StringUtils.isNotBlank(beerName) && beerStyle == null) {
            // search beer_service name
            beerPage = beerRepository.findAllByBeerName(beerName, pageRequest);
        } else if (StringUtils.isBlank(beerName) && beerStyle != null) {
            // search beer_service style
            beerPage = beerRepository.findAllByBeerStyle(beerStyle, pageRequest);
        } else {
            beerPage = beerRepository.findAll(pageRequest);
        }
        if(showInventoryOnHand) {
            beerPagedList = new BeerPagedList(
                    beerPage.getContent().stream().map(beerMapper::beerToBeerDtoWithOnHandInventory).collect(Collectors.toList()),
                    PageRequest.of(beerPage.getPageable().getPageNumber(), beerPage.getPageable().getPageSize()), beerPage.getTotalElements());
        } else {
            beerPagedList = new BeerPagedList(
                    beerPage.getContent().stream().map(beerMapper::beerToBeerDto).collect(Collectors.toList()),
                    PageRequest.of(beerPage.getPageable().getPageNumber(), beerPage.getPageable().getPageSize()), beerPage.getTotalElements());
        }
        return beerPagedList;
    }

    @Override
    public BeerDto updateBeer(UUID beerId, BeerDto beerDto) {
        Optional<Beer> beer = beerRepository.findById(beerId);
        if(beer.isPresent()) {
            Beer foundAndToBeUpdatedBeer = beer.get();
            foundAndToBeUpdatedBeer.setBeerName(beerDto.getBeerName());
            foundAndToBeUpdatedBeer.setBeerStyle(beerDto.getBeerStyle().name());
            foundAndToBeUpdatedBeer.setPrice(beerDto.getPrice());
            foundAndToBeUpdatedBeer.setUpc(beerDto.getUpc());

            return beerMapper.beerToBeerDto(beerRepository.save(foundAndToBeUpdatedBeer));
        } else {
            return null;
        }
    }

}
