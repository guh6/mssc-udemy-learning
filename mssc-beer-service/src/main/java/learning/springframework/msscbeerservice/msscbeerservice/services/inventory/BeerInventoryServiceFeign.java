//package learning.springframework.msscbeerservice.msscbeerservice.services.inventory;
//
//
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.context.annotation.Profile;
//import org.springframework.stereotype.Service;
//
//import java.util.UUID;
//
///**
// * Uses the Feign Client to make the API call
// */
//@Slf4j
//@RequiredArgsConstructor
//@Profile("local-discovery")
//@Service
//public class BeerInventoryServiceFeign  implements BeerInventoryService{
//    private final InventoryServiceFeignClient inventoryServiceFeignClient;
//
//    @Override
//    public Integer getOnhandInventory(UUID beerId) {
//        return inventoryServiceFeignClient.getOnhandInventory(beerId)
//                .getBody().getQuantityOnHand();
//    }
//}
