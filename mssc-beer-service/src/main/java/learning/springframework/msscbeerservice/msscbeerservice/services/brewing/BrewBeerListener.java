package learning.springframework.msscbeerservice.msscbeerservice.services.brewing;

import learning.springframework.msscbeerservice.msscbeerservice.config.JmsConfig;
import learning.springframework.msscbeerservice.msscbeerservice.domain.Beer;
import learning.springframework.common.events.BrewBeerEvent;
import learning.springframework.common.events.NewInventoryEvent;
import learning.springframework.msscbeerservice.msscbeerservice.repository.BeerRepository;
import learning.springframework.common.model.BeerDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import static learning.springframework.msscbeerservice.msscbeerservice.config.JmsConfig.QUEUE_NEW_INVENTORY;

@Service
@RequiredArgsConstructor
@Slf4j
/**
 * This service listens for brew-request messages and attempts to brew new beer.
 * When successful, the new inventory is sent as a message to another service.
 */
public class BrewBeerListener {
    private final BeerRepository beerRepository;
    private final JmsTemplate jmsTemplate;

    @Transactional
    @JmsListener(destination = JmsConfig.QUEUE_BREWING_REQUEST)
    public void listen(BrewBeerEvent brewBeerEvent) {
        // We could expose the quantity on hand in the DTO but for this example
        // we're going to keep it as it and use the repository
        BeerDto beerDto = brewBeerEvent.getBeerDto();
        Beer beer = beerRepository.getOne(beerDto.getId());

        log.debug("Received {}. Brewing for {}", JmsConfig.QUEUE_BREWING_REQUEST, beerDto);

        // This is just a static value to simulate brewing. We can have more
        // complicated logic in the future
        beerDto.setQuantityOnHand(beer.getQuantityToBrew());

        // Now that we have brewed the beer, we can forward the new inventory
        // to the beer-inventory-service
        NewInventoryEvent newInventoryEvent = new NewInventoryEvent(beerDto);
        jmsTemplate.convertAndSend(QUEUE_NEW_INVENTORY, newInventoryEvent);
    }
}
