//package learning.springframework.msscbeerservice.msscbeerservice.services.inventory;
//
//import lombok.RequiredArgsConstructor;
//import org.springframework.stereotype.Component;
//
//import java.util.UUID;
//
//@RequiredArgsConstructor
//@Component
//public class InventoryFailoverFeignClientImpl implements BeerInventoryService {
//
//    private final InventoryFailoverFeignClient inventoryFailoverFeignClient;
//
//    @Override
//    public Integer getOnhandInventory(UUID beerId) {
//        return inventoryFailoverFeignClient.getOnhandInventory(beerId)
//                .getBody().getQuantityOnHand();
//    }
//}
