package learning.springframework.msscbeerservice.msscbeerservice.services.inventory;

import java.util.UUID;

/**
 * Created by Guoyang Huang on 2020-11-22
 */
public interface BeerInventoryService {
    Integer getOnhandInventory(UUID beerId);
}
