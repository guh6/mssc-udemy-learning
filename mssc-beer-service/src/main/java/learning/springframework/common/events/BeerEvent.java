package learning.springframework.common.events;

import learning.springframework.common.model.BeerDto;
import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class BeerEvent implements Serializable {
    static final long serialVersionUID = -92837029203483290L;

    private BeerDto beerDto;
}
