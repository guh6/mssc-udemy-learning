GET BEER

Showing cURL:
include::{snippets}/v1/beer-get/curl-request.adoc[]

Showing HTTP:
include::{snippets}/v1/beer-get/http-request.adoc[]

Showing Response:
include::{snippets}/v1/beer-get/http-response.adoc[]

Showing Response Fields:
include::{snippets}/v1/beer-get/response-fields.adoc[]

NEW BEER

Showing cURL:
include::{snippets}/v1/beer-post/curl-request.adoc[]

Showing HTTP:
include::{snippets}/v1/beer-post/http-request.adoc[]

Showing Response:
include::{snippets}/v1/beer-post/http-response.adoc[]

Showing Request Fields:
include::{snippets}/v1/beer-post/request-fields.adoc[]