package learning.springframework.msscbeerservice.msscbeerservice.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import learning.springframework.common.model.BeerDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

@JsonTest
public class BeerDtoTest {
    @Autowired
    ObjectMapper objectMapper;

    @Test
    void testCamel() throws JsonProcessingException {
        BeerDto beerDto = BeerDto.builder()
                .createdDate(OffsetDateTime.now())
                .price(new BigDecimal("12.99"))
//                .myLocalDateForTestingJSON(OffsetDateTime.now().toLocalDate())
                .build();
        System.out.println(objectMapper.writeValueAsString(beerDto));
    }
}
