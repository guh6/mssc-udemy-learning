package learning.springframework.msscbeerservice.msscbeerservice.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import learning.springframework.msscbeerservice.msscbeerservice.services.BeerService;
import learning.springframework.common.model.BeerDto;
import learning.springframework.common.model.BeerStyleEnum;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.constraints.ConstraintDescriptions;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.BDDMockito.given;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
// Use this instead for Spring MVC Docs
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.snippet.Attributes.key;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(RestDocumentationExtension.class)
@WebMvcTest // This does NOT bring in the service layer, so we will need to add @MockBean
@AutoConfigureRestDocs(uriScheme = "https", uriHost = "learning.com", uriPort = 54321)
class BeerControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    BeerService beerService;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void getById() throws Exception {
        given(beerService.getById(any(), anyBoolean())).willReturn(getValidBeerDto());

        mockMvc.perform(get("/api/v1/beer/{beerId}", UUID.randomUUID().toString()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()) // Additional configuration below for Spring Doc
                .andDo(document("v1/beer-get",
                        pathParameters(
                                parameterWithName("beerId").description("UUID of desired beer to get.")
                        ),
                        responseFields(
                                fieldWithPath("id").description("Id of Beer").type(UUID.class),
                                fieldWithPath("version").description("TODO"),
                                fieldWithPath("createdDate").description("TODO"),
                                fieldWithPath("lastModifiedDate").description("TODO"),
                                fieldWithPath("beerName").description("TODO"),
                                fieldWithPath("beerStyle").description("TODO"),
                                fieldWithPath("upc").description("TODO"),
                                fieldWithPath("price").description("TODO"),
                                fieldWithPath("quantityOnHand").description("TODO")
                        ))
                );
    }

    @Test
    void saveNewBeer() throws Exception {
        BeerDto beerDto = getValidBeerDto();
        String beerDtoJson = objectMapper.writeValueAsString(beerDto);

        given(beerService.saveNewBeer(any())).willReturn(getValidBeerDto());

        ConstrainedFields fields = new ConstrainedFields(BeerDto.class);

        mockMvc.perform(post("/api/v1/beer/")
                        .content(beerDtoJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andDo(document("v1/beer-post",
                        requestFields(
//                                fieldWithPath("id").ignored(), // Since this is readable we can make it ignored.
//                                fieldWithPath("version").ignored(),
//                                fieldWithPath("createdDate").ignored(),
//                                fieldWithPath("lastModifiedDate").ignored(),
//                                fieldWithPath("beerName").description("TODO"),
//                                fieldWithPath("beerStyle").description("TODO"),
//                                fieldWithPath("upc").description("TODO"),
//                                fieldWithPath("price").description("TODO"),
//                                fieldWithPath("quantityOnHand").ignored()
                                fields.withPath("id").ignored(), // Since this is readable we can make it ignored.
                                fields.withPath("version").ignored(),
                                fields.withPath("createdDate").ignored(),
                                fields.withPath("lastModifiedDate").ignored(),
                                fields.withPath("beerName").description("TODO"),
                                fields.withPath("beerStyle").description("TODO"),
                                fields.withPath("upc").description("TODO"),
                                fields.withPath("price").description("TODO"),
                                fields.withPath("quantityOnHand").ignored()
                        )));
    }

    @Test
    void updateBeerById() throws Exception {
        BeerDto beerDto = getValidBeerDto();
        String beerDtoJson = objectMapper.writeValueAsString(beerDto);

        given(beerService.updateBeer(any(), any())).willReturn(getValidBeerDto());

        mockMvc.perform(put("/api/v1/beer/" + UUID.randomUUID().toString())
                        .content(beerDtoJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    private BeerDto getValidBeerDto(){
        return BeerDto.builder()
                .beerName("Test Beer")
                .beerStyle(BeerStyleEnum.IPA)
                .upc("00123456789")
                .price(new BigDecimal("1.11"))
                .build();
    }

    /**
     * This is required to use custom request-fields.snippet(mustache-template):
     *     |===
     *         |Path|Type|Description|Constraints
     *
     *     {{#fields}}
     *         |{{path}}
     *         |{{type}}
     *         |{{description}}
     *         |{{constraints}}
     *
     *     {{/fields}}
     *         |===
     *
     *  This allows it to read the Validation constraints and add to the
     *  documentations by using reflection.
     */
    public static class ConstrainedFields {
        private final ConstraintDescriptions constraintDescriptions;

        ConstrainedFields(Class<?> input) {
            this.constraintDescriptions = new ConstraintDescriptions(input);
        }

        private FieldDescriptor withPath(String path) {
            return fieldWithPath(path).attributes(key("constraints").value(StringUtils
                    .collectionToDelimitedString(this.constraintDescriptions
                            .descriptionsForProperty(path), ". ")));
        }
    }

}