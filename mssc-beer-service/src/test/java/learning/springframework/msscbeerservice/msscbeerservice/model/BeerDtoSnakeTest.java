package learning.springframework.msscbeerservice.msscbeerservice.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import learning.springframework.common.model.BeerDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.test.context.ActiveProfiles;

/**
 * This test is to demonstrate the spring properties and jackson naming strategy
 */
@ActiveProfiles("snake")
@JsonTest
public class BeerDtoSnakeTest {
    @Autowired
    ObjectMapper objectMapper;

    @Test
    void testSnake() throws JsonProcessingException {
        BeerDto beerDto = BeerDto.builder().build();
        System.out.println(objectMapper.writeValueAsString(beerDto));
    }
}
