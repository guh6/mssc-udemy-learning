package learning.springframework.beer.order.service.services.beer;

import learning.springframework.common.model.BeerDto;

import java.util.Optional;
import java.util.UUID;

public interface BeerService {
    Optional<BeerDto> getBeerById(UUID uuid);

    Optional<BeerDto> getBeerByUpc(final String upc);
}
