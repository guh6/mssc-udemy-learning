package learning.springframework.beer.order.service.web.mappers;

import learning.springframework.beer.order.service.domain.BeerOrderLine;
import learning.springframework.beer.order.service.services.beer.BeerService;
import learning.springframework.common.model.BeerDto;
import learning.springframework.common.model.BeerOrderLineDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Optional;

public abstract class BeerOrderLineMapperDecorator implements BeerOrderLineMapper{
    private BeerService beerService;
    private BeerOrderLineMapper beerOrderLineMapper;

    @Autowired
    public void setBeerService(BeerService beerService) {
        this.beerService = beerService;
    }

    @Autowired
    @Qualifier("delegate")
    public void setBeerOrderLineMapper(BeerOrderLineMapper beerOrderLineMapper) {
        this.beerOrderLineMapper = beerOrderLineMapper;
    }

    @Override
    public BeerOrderLineDto beerOrderLineToDto(BeerOrderLine line) {
        BeerOrderLineDto beerOrderLineDto = beerOrderLineMapper.beerOrderLineToDto(line);
        Optional<BeerDto> beerDto = beerService.getBeerByUpc(beerOrderLineDto.getUpc());
        beerDto.ifPresent(dto -> {
            beerOrderLineDto.setBeerName(dto.getBeerName());
            beerOrderLineDto.setBeerStyle(dto.getBeerStyle());
            beerOrderLineDto.setPrice(dto.getPrice());
            beerOrderLineDto.setBeerId(dto.getId());
        });
        return beerOrderLineDto;
    }

    @Override
    public BeerOrderLine dtoToBeerOrderLine(BeerOrderLineDto dto) {
        return beerOrderLineMapper.dtoToBeerOrderLine(dto);
    }
}
