package learning.springframework.beer.order.service.services.listeners;

import learning.springframework.beer.order.service.config.JmsConfig;
import learning.springframework.beer.order.service.services.BeerOrderManager;
import learning.springframework.common.model.AllocateOrderResult;
import learning.springframework.common.model.BeerOrderDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class BeerOrderAllocationResultListener {
    private final BeerOrderManager beerOrderManager;

    @JmsListener(destination = JmsConfig.QUEUE_ALLOCATE_ORDER_RESULT)
    public void listen(AllocateOrderResult allocateOrderResult) {
        BeerOrderDto beerOrderDto = allocateOrderResult.getBeerOrderDto();
        log.debug("Received message for queue={} for beer={}", JmsConfig.QUEUE_ALLOCATE_ORDER_RESULT, beerOrderDto);

        if(!allocateOrderResult.getAllocationError() && !allocateOrderResult.getPendingInventory()){
            // allocated normally
            beerOrderManager.beerOrderAllocationPassed(beerOrderDto);
        } else if(!allocateOrderResult.getAllocationError() && allocateOrderResult.getPendingInventory()) {
            // pending inventory
            beerOrderManager.beerOrderAllocationPendingInventory(beerOrderDto);
        } else if(allocateOrderResult.getAllocationError()) {
            // allocation error
            beerOrderManager.beerOrderAllocationFailed(beerOrderDto);
        }
    }
}
