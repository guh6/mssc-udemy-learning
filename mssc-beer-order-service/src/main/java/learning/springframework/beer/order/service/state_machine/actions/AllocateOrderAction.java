package learning.springframework.beer.order.service.state_machine.actions;

import learning.springframework.beer.order.service.config.JmsConfig;
import learning.springframework.beer.order.service.domain.BeerOrderEventEnum;
import learning.springframework.beer.order.service.domain.BeerOrderStatusEnum;
import learning.springframework.beer.order.service.repositories.BeerOrderRepository;
import learning.springframework.beer.order.service.services.BeerOrderManagerImpl;
import learning.springframework.beer.order.service.web.mappers.BeerOrderMapper;
import learning.springframework.common.model.AllocateOrderRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Slf4j
@Component
@RequiredArgsConstructor
public class AllocateOrderAction implements Action<BeerOrderStatusEnum, BeerOrderEventEnum> {
    private final BeerOrderRepository beerOrderRepository;
    private final BeerOrderMapper beerOrderMapper;
    private final JmsTemplate jmsTemplate;

    @Override
    public void execute(StateContext<BeerOrderStatusEnum, BeerOrderEventEnum> stateContext) {
        String beerOrderId = (String) stateContext.getMessage().getHeaders().get(BeerOrderManagerImpl.ORDER_ID_HEADER);
        beerOrderRepository.findById(UUID.fromString(beerOrderId)).ifPresentOrElse(beerOrder -> {
                    jmsTemplate.convertAndSend(JmsConfig.QUEUE_ALLOCATE_ORDER, AllocateOrderRequest.builder()
                            .beerOrderDto(beerOrderMapper.beerOrderToDto(beerOrder)).build());
                    log.debug("Sent message={} for beerOrder={}", JmsConfig.QUEUE_ALLOCATE_ORDER, beerOrder);
                }, () -> log.debug("BeerOrder not found for beerOrderId={}", beerOrderId));
    }
}
