package learning.springframework.beer.order.service.services;

import learning.springframework.beer.order.service.domain.BeerOrder;
import learning.springframework.beer.order.service.domain.BeerOrderEventEnum;
import learning.springframework.beer.order.service.domain.BeerOrderStatusEnum;
import learning.springframework.beer.order.service.repositories.BeerOrderRepository;
import learning.springframework.beer.order.service.state_machine.BeerOrderStateChangeInterceptor;
import learning.springframework.common.model.BeerOrderDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@Service
public class BeerOrderManagerImpl implements BeerOrderManager {
    public static final String ORDER_ID_HEADER = "ORDER_ID_HEADER";

    private final StateMachineFactory<BeerOrderStatusEnum, BeerOrderEventEnum> stateMachineFactory;
    private final BeerOrderRepository beerOrderRepository;
    private final BeerOrderStateChangeInterceptor beerOrderStateChangeInterceptor;

    @Transactional
    @Override
    public BeerOrder newBeerOrder(BeerOrder beerOrder) {
        beerOrder.setId(null); // In case the client sets the ID, this removes that
        beerOrder.setOrderStatus(BeerOrderStatusEnum.NEW);

        BeerOrder savedBeerOrder = beerOrderRepository.save(beerOrder);
        sendBeerOrderEvent(beerOrder, BeerOrderEventEnum.VALIDATE_ORDER);
        return savedBeerOrder;
    }

    @Transactional
    @Override
    public void processValidationResult(UUID orderId, Boolean isValid) {
        if(beerOrderRepository.findById(orderId).isPresent()) {
            if (isValid) {
                sendBeerOrderEvent(orderId, BeerOrderEventEnum.VALIDATION_PASS);
                sendBeerOrderEvent(orderId, BeerOrderEventEnum.ALLOCATE_ORDER);
            } else {
                sendBeerOrderEvent(orderId, BeerOrderEventEnum.VALIDATION_FAILED);
            }
        } else {
            log.debug("Beer not found to processValidationResult for orderId={}", orderId);
        }
    }

    @Override
    public void beerOrderAllocationPassed(BeerOrderDto beerOrderDto) {
        sendBeerOrderEvent(beerOrderDto.getId(), BeerOrderEventEnum.ALLOCATION_SUCCESS);
        updateAllocatedQuantity(beerOrderDto);
    }

    @Override
    public void beerOrderAllocationPendingInventory(BeerOrderDto beerOrderDto) {
        sendBeerOrderEvent(beerOrderDto.getId(), BeerOrderEventEnum.ALLOCATION_NO_INVENTORY);
        updateAllocatedQuantity(beerOrderDto);
    }

    @Override
    public void beerOrderAllocationFailed(BeerOrderDto beerOrderDto) {
        sendBeerOrderEvent(beerOrderDto.getId(), BeerOrderEventEnum.ALLOCATION_FAILED);
    }

    @Override
    public void cancelOrder(UUID beerOrderId) {
        beerOrderRepository.findById(beerOrderId).ifPresentOrElse(beerOrder -> {
            sendBeerOrderEvent(beerOrder, BeerOrderEventEnum.CANCEL_ORDER);
        }, () -> log.error("BeerOrder not found for beerOrderPickedUp for beerOrder={}", beerOrderId));
    }

    @Override
    public void beerOrderPickedUp(UUID beerOrderId) {
        beerOrderRepository.findById(beerOrderId).ifPresentOrElse(beerOrder -> {
            sendBeerOrderEvent(beerOrder, BeerOrderEventEnum.BEERORDER_PICKED_UP);
        }, () -> log.error("BeerOrder not found for beerOrderPickedUp for beerOrder={}", beerOrderId));
    }

    private void updateAllocatedQuantity(BeerOrderDto beerOrderDto) {
        UUID beerOrderId = beerOrderDto.getId();
        Optional<BeerOrder> optionalAllocatedOrder = beerOrderRepository.findById(beerOrderId);
        if(optionalAllocatedOrder.isPresent()) {
            BeerOrder allocatedOrder = optionalAllocatedOrder.get();
            allocatedOrder.getBeerOrderLines().forEach(beerOrderLine -> {
                beerOrderDto.getBeerOrderLines().forEach(beerOrderLineDto -> {
                    if (beerOrderLine.getId().equals(beerOrderLineDto.getId())) {
                        beerOrderLine.setQuantityAllocated(beerOrderLineDto.getQuantityAllocated());
                    }
                });
            });
            beerOrderRepository.saveAndFlush(allocatedOrder);
        } else {
            log.debug("BeerOrder not found for updateAllocatedQuantity for beerOrderDto={}",
                    beerOrderDto);
        }

    }

    /**
     * Sending the event to the state machine
     * 1. Create a state machine from the beer order
     * 2. Since this is a new order, a VALIDATE_ORDER event is sent to the state machine
     * @param beerOrder
     * @param beerOrderEventEnum
     */
    private void sendBeerOrderEvent(BeerOrder beerOrder, BeerOrderEventEnum beerOrderEventEnum) {
        StateMachine<BeerOrderStatusEnum, BeerOrderEventEnum> sm = buildSMForBeerOrder(beerOrder);
        Message messageEvent = MessageBuilder.withPayload(beerOrderEventEnum)
                .setHeader(ORDER_ID_HEADER, beerOrder.getId().toString())
                .build();
        sm.sendEvent(messageEvent);
    }

    /**
     * Sending the event to the state machine
     * 1. Create a state machine from the beer order
     * 2. Since this is a new order, a VALIDATE_ORDER event is sent to the state machine
     * @param beerOrderId
     * @param beerOrderEventEnum
     */
    private void sendBeerOrderEvent(UUID beerOrderId, BeerOrderEventEnum beerOrderEventEnum) {
        beerOrderRepository.findById(beerOrderId)
                .ifPresent(beerOrder -> sendBeerOrderEvent(beerOrder, beerOrderEventEnum));
    }

    /**
     * Rehydrates the beer order from the database using the entity. Also adds
     * an interceptor to automatically write the state to the db iff state changes.
     * @param beerOrder
     * @return
     */
    private StateMachine<BeerOrderStatusEnum, BeerOrderEventEnum> buildSMForBeerOrder(BeerOrder beerOrder) {
        // Retrieves a sm if it exists, otherwise it will build one
        StateMachine<BeerOrderStatusEnum, BeerOrderEventEnum> sm = stateMachineFactory.getStateMachine(beerOrder.getId());
        sm.stop();

        sm.getStateMachineAccessor().doWithAllRegions(sma-> {
            sma.addStateMachineInterceptor(beerOrderStateChangeInterceptor);
            sma.resetStateMachine(new DefaultStateMachineContext<>(beerOrder.getOrderStatus(), null, null, null));
        });
        sm.start();

        return sm;
    }

}
