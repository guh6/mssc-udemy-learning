package learning.springframework.beer.order.service.state_machine;

import learning.springframework.beer.order.service.domain.BeerOrder;
import learning.springframework.beer.order.service.domain.BeerOrderEventEnum;
import learning.springframework.beer.order.service.domain.BeerOrderStatusEnum;
import learning.springframework.beer.order.service.repositories.BeerOrderRepository;
import learning.springframework.beer.order.service.services.BeerOrderManagerImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.support.StateMachineInterceptorAdapter;
import org.springframework.statemachine.transition.Transition;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Slf4j
@Component
@RequiredArgsConstructor
public class BeerOrderStateChangeInterceptor extends StateMachineInterceptorAdapter<BeerOrderStatusEnum, BeerOrderEventEnum> {
    private final BeerOrderRepository beerOrderRepository;

    /**
     * When message is received, the order id is pulled off of the message,
     * retrieve the entity from the db and write the state back to the db.
     * @param state
     * @param message
     * @param transition
     * @param stateMachine
     */
    @Override
    public void preStateChange(State<BeerOrderStatusEnum, BeerOrderEventEnum> state,
                                Message<BeerOrderEventEnum> message,
                                Transition<BeerOrderStatusEnum,  BeerOrderEventEnum> transition,
                                StateMachine<BeerOrderStatusEnum, BeerOrderEventEnum> stateMachine) {
        if(message != null) {
            String id = (String) message.getHeaders().getOrDefault(BeerOrderManagerImpl.ORDER_ID_HEADER, " ");
            if(id != null) {
                log.debug("Saving state for order id={} and status={}", id, state.getId());
                BeerOrder beerOrder = beerOrderRepository.getOne(UUID.fromString(id));
                beerOrder.setOrderStatus(state.getId());
                beerOrderRepository.saveAndFlush(beerOrder); // Hibernate's default is lazy write
            }
        }

        super.postStateChange(state, message, transition, stateMachine);
    }
}
