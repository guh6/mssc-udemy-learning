package learning.springframework.beer.order.service.state_machine.actions;

import learning.springframework.beer.order.service.config.JmsConfig;
import learning.springframework.beer.order.service.domain.BeerOrder;
import learning.springframework.beer.order.service.domain.BeerOrderEventEnum;
import learning.springframework.beer.order.service.domain.BeerOrderStatusEnum;
import learning.springframework.beer.order.service.repositories.BeerOrderRepository;
import learning.springframework.beer.order.service.services.BeerOrderManagerImpl;
import learning.springframework.beer.order.service.web.mappers.BeerOrderMapper;
import learning.springframework.common.model.DeallocateOrderRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Slf4j
@Component
@RequiredArgsConstructor
public class DeallocateOrderAction implements Action<BeerOrderStatusEnum, BeerOrderEventEnum> {
    private final JmsTemplate jmsTemplate;
    private final BeerOrderRepository beerOrderRepository;
    private final BeerOrderMapper beerOrderMapper;

    @Override
    public void execute(StateContext<BeerOrderStatusEnum, BeerOrderEventEnum> stateContext) {
        String beerOrderId = (String) stateContext.getMessage().getHeaders().get(BeerOrderManagerImpl.ORDER_ID_HEADER);
        Optional<BeerOrder> beerOrderOptional = beerOrderRepository.findById(UUID.fromString(beerOrderId));

        beerOrderOptional.ifPresentOrElse(beerOrder -> {
            jmsTemplate.convertAndSend(JmsConfig.QUEUE_DEALLOCATE_ORDER,
                    DeallocateOrderRequest.builder()
                            .beerOrderDto(beerOrderMapper.beerOrderToDto(beerOrder))
                            .build());
            log.debug("Sent deallocation request for order id={}", beerOrderId);
        }, () -> log.error("Beer order not found"));
    }
}
