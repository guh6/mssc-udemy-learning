package learning.springframework.beer.order.service.services;

import learning.springframework.beer.order.service.domain.BeerOrder;
import learning.springframework.common.model.BeerOrderDto;

import java.util.UUID;

public interface BeerOrderManager {

    /**
     * Creates a new beer and kicks off the validation process
     * @param beerOrder
     * @return
     */
    BeerOrder newBeerOrder(BeerOrder beerOrder);

    void processValidationResult(UUID orderId, Boolean isValid);

    void beerOrderAllocationPassed(BeerOrderDto beerOrderDto);

    void beerOrderAllocationPendingInventory(BeerOrderDto beerOrderDto);

    void beerOrderAllocationFailed(BeerOrderDto beerOrderDto);

    void cancelOrder(UUID id);

    void beerOrderPickedUp(UUID id);
}
