package learning.springframework.beer.order.service.services.listeners;

import learning.springframework.beer.order.service.config.JmsConfig;
import learning.springframework.common.model.ValidateOrderResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Slf4j
@RequiredArgsConstructor
@Service
public class CancelOrderListener {

    @Transactional
    @JmsListener(destination = JmsConfig.QUEUE_DEALLOCATE_ORDER)
    public void listen(ValidateOrderResult validateOrderResult) {
        log.debug("Received {} response for {}", JmsConfig.QUEUE_VALIDATE_ORDER_RESULT, validateOrderResult);
        // TODO: Have another service to deal with the cancel order
    }
}
