package learning.springframework.beer.order.service.services.beer;

import learning.springframework.common.model.BeerDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;
import java.util.UUID;

@Service
public class BeerServiceImpl implements BeerService {
    public static final String BEER_PATH = "/api/v1/beer/";
    public static final String BEER_UPC_PATH = "/api/v1/beer/beerUpc/";
    private final RestTemplate restTemplate;
    private final String beerServiceHost;

    public BeerServiceImpl(RestTemplateBuilder restTemplateBuilder,
                           @Value("${sfg.brewery.beer-service.user}") final String user,
                           @Value("${sfg.brewery.beer-service.password}") final String password,
                           @Value("${sfg.brewery.beer-service.hostname}") final String hostname) {
        this.restTemplate = restTemplateBuilder
                .basicAuthentication(user, password).build();
        this.beerServiceHost = hostname;
    }

    @Override
    public Optional<BeerDto> getBeerById(UUID uuid) {
        return Optional.of(restTemplate.getForObject(beerServiceHost + BEER_PATH + uuid.toString(), BeerDto.class));
    }

    @Override
    public Optional<BeerDto> getBeerByUpc(String upc) {
        return Optional.of(restTemplate.getForObject(beerServiceHost + BEER_UPC_PATH + upc, BeerDto.class));
    }
}
