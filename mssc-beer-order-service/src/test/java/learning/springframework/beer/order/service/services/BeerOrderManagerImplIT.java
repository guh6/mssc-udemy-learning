package learning.springframework.beer.order.service.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.jenspiegsa.wiremockextension.WireMockExtension;
import com.github.tomakehurst.wiremock.WireMockServer;
import learning.springframework.beer.order.service.domain.BeerOrder;
import learning.springframework.beer.order.service.domain.BeerOrderLine;
import learning.springframework.beer.order.service.domain.BeerOrderStatusEnum;
import learning.springframework.beer.order.service.domain.Customer;
import learning.springframework.beer.order.service.repositories.BeerOrderRepository;
import learning.springframework.beer.order.service.repositories.CustomerRepository;
import learning.springframework.beer.order.service.services.beer.BeerServiceImpl;
import learning.springframework.common.model.BeerDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static com.github.jenspiegsa.wiremockextension.ManagedWireMockServer.with;
import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(WireMockExtension.class)
@SpringBootTest
public class BeerOrderManagerImplIT {
    @Autowired
    private BeerOrderManager beerOrderManager;

    @Autowired
    private BeerOrderRepository beerOrderRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    WireMockServer wireMockServer;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private Customer testCustomer;

    private final UUID testBeerId = UUID.randomUUID();
    private final String testBeerUPC = "12345";

    /**
     * Creates a WireMock Server
     */
    @TestConfiguration
    static class RestTemplateBuilderProvider {
        @Bean(destroyMethod = "stop")
        public WireMockServer wireMockServer(){
            WireMockServer server = with(wireMockConfig().port(8083));
            server.start();
            return server;
        }
    }

    @BeforeEach
    void setup(){
        testCustomer = customerRepository.save(Customer.builder()
            .customerName("Test Customer").build());
    }

    @Test
    // TODO 1: need to figure out why it's iterating through the list of UPCS instead of the predefined testBeer
    // TODO 2: need to figure out why it's not moving from ALLOCATION_SUCCESS event
    void test_NewToAllocate() throws JsonProcessingException, InterruptedException {
        BeerDto beerDto = BeerDto.builder().id(testBeerId)
                .upc(testBeerUPC).build();

        // Stubs the request using WireMock
        wireMockServer.stubFor(any(urlPathMatching(BeerServiceImpl.BEER_UPC_PATH + "([a-z0-9]*)"))
                .willReturn(okJson(objectMapper.writeValueAsString(beerDto))));

        // Test the manager
        BeerOrder beerOrder = createBeerOrder();
        BeerOrder savedBeerOrder = beerOrderManager.newBeerOrder(beerOrder);

        // Prevent the test exiting too soon after processing jms message
        // Thread.sleep(5000);
        await().untilAsserted(() -> {
            BeerOrder foundBeerOrder = beerOrderRepository.findById(beerOrder.getId()).get();
            assertEquals(BeerOrderStatusEnum.ALLOCATED, foundBeerOrder.getOrderStatus());
        });

        assertNotNull(savedBeerOrder);
        assertEquals(BeerOrderStatusEnum.ALLOCATED, savedBeerOrder.getOrderStatus());
    }

    @Test
    void test_FailedValidation() throws JsonProcessingException {
        BeerDto beerDto = BeerDto.builder().id(testBeerId).upc(testBeerUPC).build();
        wireMockServer.stubFor(any(urlPathMatching(BeerServiceImpl.BEER_UPC_PATH + "([a-z0-9]*)"))
                .willReturn(okJson(objectMapper.writeValueAsString(beerDto))));
        BeerOrder beerOrder = createBeerOrder();
        beerOrder.setCustomerRef("fail-validation");

        BeerOrder savedBeerOrder = beerOrderManager.newBeerOrder(beerOrder);

        await().untilAsserted(()->{
            BeerOrder foundOrder = beerOrderRepository.findById(beerOrder.getId()).get();
            assertEquals(BeerOrderStatusEnum.VALIDATION_EXCEPTION, savedBeerOrder.getOrderStatus());
        });
    }

    private BeerOrder createBeerOrder(){
        BeerOrder beerOrder = BeerOrder.builder()
                .customer(testCustomer).build();

        Set<BeerOrderLine> lines = new HashSet<>();
        lines.add(BeerOrderLine.builder()
                .upc(testBeerUPC)
                .beerId(testBeerId)
                .orderQuantity(1)
                .beerOrder(beerOrder).build());

        beerOrder.setBeerOrderLines(lines);

        return beerOrder;
    }
}
