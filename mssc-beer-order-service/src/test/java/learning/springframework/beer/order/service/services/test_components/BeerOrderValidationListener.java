package learning.springframework.beer.order.service.services.test_components;

import learning.springframework.beer.order.service.config.JmsConfig;
import learning.springframework.common.model.ValidateBeerOrderRequest;
import learning.springframework.common.model.ValidateOrderResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

/**
 * Stub to listen for the message
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class BeerOrderValidationListener {

    private final JmsTemplate jmsTemplate;

    @JmsListener(destination = JmsConfig.QUEUE_VALIDATE_ORDER)
    public void listen(Message msg) {
        boolean isValid = true;
        boolean sendResponse = true;

        ValidateBeerOrderRequest request = (ValidateBeerOrderRequest) msg.getPayload();

        String customerRef = request.getBeerOrderDto().getCustomerRef();
        if("fail-validation".equals(customerRef)) {
            isValid = false;
        } else if("dont-validate".equals(customerRef)) {
            sendResponse = false;
        }

        if(sendResponse) {
            System.out.println("Sending the stubbed listener to mock validate order result queue for " + request.getBeerOrderDto());
            jmsTemplate.convertAndSend(JmsConfig.QUEUE_VALIDATE_ORDER_RESULT,
                    ValidateOrderResult.builder()
                            .isValid(isValid)
                            .orderId(request.getBeerOrderDto().getId()).build());
        }
    }
}
