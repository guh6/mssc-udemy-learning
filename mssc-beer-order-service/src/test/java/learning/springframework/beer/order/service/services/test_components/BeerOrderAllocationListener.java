package learning.springframework.beer.order.service.services.test_components;

import learning.springframework.beer.order.service.config.JmsConfig;
import learning.springframework.common.model.AllocateOrderRequest;
import learning.springframework.common.model.AllocateOrderResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class BeerOrderAllocationListener {

//    private final JmsTemplate jmsTemplate;
//
//    @JmsListener(destination = JmsConfig.QUEUE_VALIDATE_ORDER)
//    public void listen(Message msg) {
//        ValidateBeerOrderRequest request = (ValidateBeerOrderRequest) msg.getPayload();
//
//
//        request.getBeerOrderDto().getBeerOrderLines().forEach(beerOrderLineDto -> {
//            beerOrderLineDto.setQuantityAllocated(beerOrderLineDto.getOrderQuantity());
//        });
//        System.out.println("Sending stub message from BeerOrderAllocationListener for " + request.getBeerOrderDto());
//        jmsTemplate.convertAndSend(JmsConfig.QUEUE_ALLOCATE_ORDER_RESULT,
//        AllocateOrderResult.builder().beerOrderDto(request.getBeerOrderDto())
//                .allocationError(false)
//                .pendingInventory(false)
//                .build());
//    }
    private final JmsTemplate jmsTemplate;

    @JmsListener(destination = JmsConfig.QUEUE_ALLOCATE_ORDER)
    public void listen(Message msg){
        AllocateOrderRequest request = (AllocateOrderRequest) msg.getPayload();
        boolean pendingInventory = false;
        boolean allocationError = false;
        boolean sendResponse = true;

        //set allocation error
        if (request.getBeerOrderDto().getCustomerRef() != null) {
            if (request.getBeerOrderDto().getCustomerRef().equals("fail-allocation")){
                allocationError = true;
            }  else if (request.getBeerOrderDto().getCustomerRef().equals("partial-allocation")) {
                pendingInventory = true;
            } else if (request.getBeerOrderDto().getCustomerRef().equals("dont-allocate")){
                sendResponse = false;
            }
        }

        boolean finalPendingInventory = pendingInventory;

        request.getBeerOrderDto().getBeerOrderLines().forEach(beerOrderLineDto -> {
            if (finalPendingInventory) {
                beerOrderLineDto.setQuantityAllocated(beerOrderLineDto.getOrderQuantity() - 1);
            } else {
                beerOrderLineDto.setQuantityAllocated(beerOrderLineDto.getOrderQuantity());
            }
        });

        if (sendResponse) {
            jmsTemplate.convertAndSend(JmsConfig.QUEUE_ALLOCATE_ORDER_RESULT,
                    AllocateOrderResult.builder()
                            .beerOrderDto(request.getBeerOrderDto())
                            .pendingInventory(pendingInventory)
                            .allocationError(allocationError)
                            .build());
        }
    }
}
