# Microservices with Spring Cloud Udemy Learning #

This repository contains code by doing assignments in the Udemy course. Each subdirectory is a microservice that can be independently opened.

MSSC = Microservices with Spring Cloud.

---
# Microservices #
## Technologies ##
* Spring Framework
* EhCache
* H2/MySQL for JPA
* Maven BOM
* Spring Cloud Gateway
* Eureka
* Spring Cloud Config Server
* Spring State Machine


## brewery-monolith ##
This is a working example of how to deconstruct a monolith into individual microservices.

## Deconstructed Microservices ##
### mssc-beer-inventory-service ###
This is a microservice split from brewery-monolith. Contains example of WireMock. TODO doc

### mssc-beer-order-service ###
This is a microservice split from brewery-monolith. TODO doc

### mssc-beer-service ###
This is a microservice split from brewery-monolith. TODO doc

### mssc-gateway ###
Gateway Service. TODO doc

## Configurations ##
TODO
## Service ports ##
| Service | Port |
|---|---|
| Beer Service | 8080 |
| Beer Order Service | 8081 |
| Beer Inventory Service | 8082 |
| Beer Gateway Service | 9090 |
| Eureka Server | 8761 |
| Beer Gateway Service | 9090 |

## Local Containers ##
TODO: docker-compose

### MySQL ###
From https://hub.docker.com/_/mysql
```shell
docker run -d --name local_mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=secret mysql:8.0 
```

### ActiveMQ ###
From https://github.com/vromero/activemq-artemis-docker
```shell
docker run -d --rm --name local_activemq -p 8161:8161 -p 61616:61616 -e ARTEMIS_USERNAME=admin -e ARTEMIS_PASSWORD=changeit vromero/activemq-artemis
```

### Zipkin ###
```shell
docker run -d --name local_zipkin -p 9411:9411 openzipkin/zipkin
```

Then go to `http://localhost:9411/zipkin/`.

---

## Demo Projects ##
### mssc-brewery-bom ###
Project demonstrating:
*  Use of Maven BOM(Bill of Materials). 
*  This is also used in the microservices referencing this BOM.

### mssc-brewery-client ###
Project demonstrating:
*  Use of Rest Template.
*  Best practice on configuring Rest Template with Connection Pooling.

### mssc-brewery ###
Project demonstrating:
*  REST APIs
*  MapStruct Mappers
*  API versioning from v1 to v2

### mssc-jms ###
Project demonstrating:
*  Use of Java Messaging Service using ActiveMQ Artemis.  

### mssc-ssm ###
Project on Spring State Machine that demonstrates:
*  Use of Spring State machine

### mssc-config-server ###
Project on Spring Cloud Config Server.

---
# Notes from Course #
## Useful tools ##
* Axis TCP Monitor – an Intellij Plugin that acts as the frontend facing interface for the Spring boot application. This is useful for looking at the raw TCP client requests and application responses.
* Spring Boot Developer Tools – automatically disabled when running a packaged application (ie java -jar). Restarts are very fast since only project classes are bring loaded.
* Browser Plugins are available at livereload.com

## Things to know ##
*  Networking communication is done in java.io
*  Socket is cheaper to create than threads (100,000s sockets vs 10,000s threads)
*  Non-blocking clients is usually better than blocking. 
*  Blocking: threads go to sleep/idle then has to be brought back
    *  Java’s implementation
    *  Apache HTTP Client
    *  Jersey
    *  OkHttp
* Non-blocking: always active
    *  Apache Async
    *  Jersey Async
    *  Netty (Spring’s default)
* HTTP/2
    *  More performant than HTTP 1.1
    *  Clients: Java 9+, Jetty, Netty, OkHttp, Vert.x, Firefly, Apache 5.x
* Create a RestTemplate Customizer so that you can:
    *  Have Connection Pool Manager
    *  Better Performance
* Validation
   * Should be used in your tool set for defensive programming.
   * Lets the consumer know what is wrong early
* Lombok & MapStruct
* MapStruct
    * code generator for mapping between Java bean types
    * Convention over configuration
    * Follow https://github.com/mapstruct/mapstruct to configure MapStruct
    * https://mapstruct.org/documentation/stable/reference/html/ 
* Spring MVC RestDocs
    * Hooks into controller tests to generate documentation snippets via Asciidoctor
* Maven BOM (Bill of materials)
    * Centralized location for dependencies across multiple projects (microservices)
    * For cases like fixing security patches for an existing library and following compliance
    * Allows for auditing 
* JMS
    * Like JPA, this is a standard for communication between Java processes.
    * Faster than HTTP
* Spring State machine
    * Spring uses spring-messaging
* Sagas
    * CAP Theorem - Can have two of the characteristics
    * BASE - an ACID alternative. Basically Available, Soft state, Eventually consistent
    * Introduced in 1987
    * A series of steps to complete a business process by coordinating invocation of microservices via messages or requests (asynchronous)
    * Should be idempotent
    * ACD of ACID principal; BASE
    * Coordination can be choreography(distributed decision making) or orchestration(centralized decision making). Use coordination for smaller simple sagas. Use orchestration for complex sagas.
* WireMock
    * Useful for stubbing external service callsnn
* Awaitility
    * Useful for testing synchronous actions without having explicit sleep time
* Spring Cloud Contract
    * https://spring.io/projects/spring-cloud-contract
    * Consumer-driven contract
    * Stubs for usability
* API Gateway Pattern
    * Responsibilities:
      * Routing
      * Security
      * Rate Limiting
      * Monitoring/logging
      * blue/green deploys
      * caching
      * monolith strangling
* Circuit Breaker
    * Resilience4J is favored over Hystrix
* Docker for JVM
    * OpenJDK Slim
    * Fabric8
    * Azul