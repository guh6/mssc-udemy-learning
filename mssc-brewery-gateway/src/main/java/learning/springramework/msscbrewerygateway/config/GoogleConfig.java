package learning.springramework.msscbrewerygateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;


/**
 * Demos redirecting Google searches with path rewrite
 */
@Profile("google")
@Configuration
public class GoogleConfig {
    @Bean
    public RouteLocator googleRouteConfig(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(r-> r.path("/googlesearch")
                        .filters(f-> f.rewritePath("/googlesearch2(?<segment>/?.*)", "/$(segement)"))
                .uri("https://google.com")).build();
    }
}
