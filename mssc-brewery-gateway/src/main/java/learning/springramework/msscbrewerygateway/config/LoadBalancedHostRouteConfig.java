package learning.springramework.msscbrewerygateway.config;

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableDiscoveryClient
@Configuration
public class LoadBalancedHostRouteConfig {

    @Bean
    public RouteLocator loadBalancedHostRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(r-> r.path("/api/v1/beer/*", "/api/v1/beer*", "/api/v1/beerUpc/*")
                        .uri("lb://beer-service"))
                .route(r-> r.path("/api/v1/customer/**")
                        .uri("lb://beer-order-service"))
                .route(r-> r.path("/api/v1/beer/*/inventory")
                        // TODO: create separate service just for dealing with fallback
                        .filters(f -> f.circuitBreaker(c -> c.setName("inventoryCB")
                                .setFallbackUri("forward:/another-service-here")
                                .setRouteId("inv-fallback")))
                        .uri("lb://beer-inventory-service"))
                .build();
    }

}
